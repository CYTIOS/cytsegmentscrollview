//
//  SecondViewController.h
//  CYTSegmentDemo
//
//  Created by hudi on 2017/10/23.
//  Copyright © 2017年 hudi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CYTSegmentScrollView.h"

@interface SegmentScrollViewController : UIViewController
@property (nonatomic, assign) CYTSegmentType mSegmentType;
@property (nonatomic, assign) NSInteger index;
@end
