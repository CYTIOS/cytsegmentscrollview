//
//  UIView+Frame.h
//  HDProject
//
//  Created by 胡迪 on 14/12/31.
//  Copyright (c) 2014年 com.netgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)
@property (nonatomic,assign)double width_HD;
@property (nonatomic,assign)double height_HD;
@property (nonatomic,assign)double originX_HD;
@property (nonatomic,assign)double originY_HD;
@property (nonatomic,assign)double centerX_HD;
@property (nonatomic,assign)double centerY_HD;


/**
 *  绕某点旋转
 *
 *  @param centerX 中心
 *  @param centerY 中心
 *  @param x       旋转x
 *  @param y       旋转x
 *  @param angle   角度
 */
-(void)GetCGAffineTransformRotateAroundPoint:(float)centerX centerY:(float)centerY x:(float) x y:(float) y angle:(float) angle;

/**
 *  放大缩小
 *
 *  @param h 横向倍数
 *  @param v 纵向倍数
 */
- (void)transformScale:(CGFloat)h v:(CGFloat)v;
@end
