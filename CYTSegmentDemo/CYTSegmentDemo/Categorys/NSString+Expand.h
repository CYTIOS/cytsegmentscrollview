//
//  NSString+Expand.h
//  Cyt-BabyHealth
//
//  Created by 女巫 on 16/7/14.
//  Copyright © 2016年 CYT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Expand)

/**
 *  判断字符串是否为nil
 *
 *  @param str 待判断的字符串
 *
 *  @return YES/NO
 */
+(BOOL)isnil:(NSString *)str;

/**
 *  转大写金额
 *
 *  @param amount 金额
 *
 *  @return 大写金额
 */
+ (NSString *)digitUppercase:(double )amount;


/**
 *  //获取字符串的CGSize
 *
 *  @param font     字体
 *  @param maxWidth 最大宽度
 *
 *  @return 所占的CGSize
 */
-(CGSize)getSizeoffont:(UIFont *)font withMaxWidth:(double)maxWidth;


/**
 *  所占字节数
 *
 *  @return 字节数
 */
- (int)getbyte;
-(NSUInteger)unicodeLength;

/**
 *  url编码
 *
 *  @return 编码后的结果
 */
- (NSString*)stringByURLEncoding;



/**
 *  数组字典转json串
 *
 *  @param obj 数组字典
 *
 *  @return json串
 */
+ (NSString *)toJSON:(id)obj;

/**
 *  json串转数组字典
 *
 *  @return
 */
- (id)toArrayOrNSDictionary;

- (NSString *)toUtf8;
+ (NSString *)utf8ToUnicode:(NSString *)string;

/**
 *  返回 字符串 长度 （计算字节）方法一
 *
 *  @return
 */

+ (int)convertToInt:(NSString*)strtemp;
/**
 *  返回 字符串 长度 （计算字节）方法二
 *
 *  @return
 */
+(NSUInteger)uniCodeLengthOfString:(NSString *)text;
@end
