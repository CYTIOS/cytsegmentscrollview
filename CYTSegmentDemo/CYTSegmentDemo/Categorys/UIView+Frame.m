//
//  UIView+Frame.m
//  HDProject
//
//  Created by 胡迪 on 14/12/31.
//  Copyright (c) 2014年 com.netgen. All rights reserved.
//

#import "UIView+Frame.h"
#import <objc/runtime.h>
@implementation UIView (Frame)
- (void)setOriginX_HD:(double)originX_HD
{
    CGRect frame=self.frame;
    frame.origin.x=originX_HD;
    self.frame=frame;
}

-(void)setOriginY_HD:(double)originY_HD
{
    CGRect frame=self.frame;
    frame.origin.y=originY_HD;
    self.frame=frame;
}

-(void)setHeight_HD:(double)height_HD
{
    CGRect frame=self.frame;
    frame.size.height=height_HD;
    self.frame=frame;
}

-(void)setWidth_HD:(double)width_HD
{
    CGRect frame=self.frame;
    frame.size.width=width_HD;
    self.frame=frame;
}

-(double)width_HD
{
    return self.frame.size.width;
}

- (double)height_HD
{
    return self.frame.size.height;
}

- (double)originX_HD
{
    return self.frame.origin.x;
}

- (double)originY_HD
{
    return self.frame.origin.y;
}


- (void)setCenterX_HD:(double)centerX_HD
{
    self.center=CGPointMake(centerX_HD, self.center.y);
}

- (double)centerX_HD
{
   return self.center.x;
}

- (void)setCenterY_HD:(double)centerY_HD
{
    self.center=CGPointMake(self.center.x, centerY_HD);
}

- (double)centerY_HD
{
    return self.center.y;
}

-(void)GetCGAffineTransformRotateAroundPoint:(float)centerX centerY:(float)centerY x:(float) x y:(float) y angle:(float) angle
{
    x = x - centerX; //计算(x,y)从(0,0)为原点的坐标系变换到(CenterX ，CenterY)为原点的坐标系下的坐标
    y = y - centerY; //(0，0)坐标系的右横轴、下竖轴是正轴,(CenterX,CenterY)坐标系的正轴也一样
    
    CGAffineTransform  trans = CGAffineTransformMakeTranslation(x, y);
    trans = CGAffineTransformRotate(trans,angle);
    trans = CGAffineTransformTranslate(trans,-x, -y);
    self.transform = CGAffineTransformIdentity;
    self.transform = trans;
}

- (void)transformScale:(CGFloat)h v:(CGFloat)v
{
    CGAffineTransform transform = self.transform;
    transform = CGAffineTransformScale(transform, h,v);//前面的2表示横向放大2倍，后边的0.5表示纵向缩小一半
    self.transform = transform;
}
@end
