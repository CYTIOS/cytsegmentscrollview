//
//  NSString+Expand.m
//  Cyt-BabyHealth
//
//  Created by 女巫 on 16/7/14.
//  Copyright © 2016年 CYT. All rights reserved.
//

#import "NSString+Expand.h"

@implementation NSString (Expand)

+(BOOL)isnil:(NSString *)str{
    if ([str isKindOfClass:[NSNull class]]) {
        //  NSLog(@"[NSNull class]");
        return YES;
    }
    if ([str isEqual:[NSNull null]]) {
        //  NSLog(@"[NSNull null]");
        return YES;
    }
    if (str==nil) {
        // NSLog(@"nil");
        return YES;
    }
    if ([str isEqualToString:@""]) {
        // NSLog(@"");
        return YES;
    }
    if ([str isEqualToString:@"null"]) {
        // NSLog(@"");
        return YES;
    }
    return NO;
}


#pragma mark ======金额转大写======
+ (NSString *)partTranslate:(int)amountPart{
    NSMutableArray * chineseDigits=[NSMutableArray arrayWithObjects:@"零",@"壹", @"贰",@"叁",@"肆",@"伍",@"陆",@"柒",@"捌",@"玖",nil];
    if (amountPart < 0 || amountPart > 10000) {
        
        NSLog(@"参数必须是大于等于 0，小于 10000 的整数！");
        
    }
    NSMutableArray *units=[NSMutableArray arrayWithObjects:@"",@"拾",@"佰",@"仟", nil];
    
    
    int temp = amountPart;
    
    NSString *amountStr =[NSString stringWithFormat:@"%d",amountPart];
    NSInteger amountStrLength = amountStr.length;
    
    BOOL lastIsZero = true; // 在从低位往高位循环时，记录上一位数字是不是 0
    
    NSString *chineseStr = @"";
    
    for (int i = 0; i < amountStrLength; i++) {
        
        if (temp == 0) // 高位已无数据
            
            break;
        
        int digit = temp % 10;
        
        if (digit == 0) { // 取到的数字为 0
            
            if (!lastIsZero) // 前一个数字不是 0，则在当前汉字串前加“零”字;
                
                chineseStr =[NSString stringWithFormat:@"零%@",chineseStr];
            
            lastIsZero = true;
            
        }
        
        else { // 取到的数字不是 0
            
            chineseStr =[NSString stringWithFormat:@"%@%@%@",[chineseDigits objectAtIndex:digit],[units objectAtIndex:i],chineseStr];
            lastIsZero = false;
            
        }
        
        temp = temp / 10;
        
    }
    
    return chineseStr;
    
}

+ (NSString *)digitUppercase:(double)amount{
    NSMutableArray * chineseDigits=[NSMutableArray arrayWithObjects:@"零",@"壹", @"贰",@"叁",@"肆",@"伍",@"陆",@"柒",@"捌",@"玖",nil];
    BOOL negative = false;
    if(amount<0){
        negative=true;
        amount=amount*(-1);
    }
    long temp=amount*100;
    int numFen = (int) (temp % 10); // 分
    
    temp = temp / 10;
    int numJiao = (int) (temp % 10); // 角
    
    temp = temp / 10;
    int parts[20]; // 其中的元素是把原来金额整数部分分割为值在 0~9999 之间的数的各个部分
    
    int numParts = 0; // 记录把原来金额整数部分分割为了几个部分（每部分都在 0~9999 之间）
    // temp 目前是金额的整数部分
    for (int i = 0;; i++) {
        if (temp == 0)
            break;
        int part = (int) (temp % 10000);
        parts[i] = part;
        numParts++;
        temp = temp / 10000;
    }
    
    BOOL beforeWanIsZero=true;
    NSString *chineseStr=@"";
    for (int i = 0; i < numParts; i++) {
        NSString *partChinese = [self partTranslate:parts[i]];
        //NSString *partChinese = @"";
        
        if (i % 2 == 0) {
            if([@"" isEqualToString:partChinese]){
                beforeWanIsZero = true;
            }else
                beforeWanIsZero = false;
        }
        if (i != 0) {
            if (i % 2 == 0)
                chineseStr =[NSString stringWithFormat:@"亿%@",chineseStr];
            
            else {
                
                if ([@"" isEqualToString:partChinese] && !beforeWanIsZero) // 如果“万”对应的
                    // part 为
                    // 0，而“万”下面一级不为
                    // 0，则不加“万”，而加“零”
                    
                    chineseStr =[NSString stringWithFormat:@"零%@",chineseStr];
                else {
                    NSLog(@"我勒个去--->%d--->",parts[i-1]);
                    if (parts[i - 1] < 1000 && parts[i - 1] > 0) // 如果"万"的部分不为
                        // 0,
                        // 而"万"前面的部分小于
                        // 1000
                        // 大于 0，
                        // 则万后面应该跟“零”
                        
                        chineseStr =[NSString stringWithFormat:@"零%@",chineseStr];
                    chineseStr =[NSString stringWithFormat:@"万%@",chineseStr];
                }
            }
        }
        NSLog(@"------------<<>>>>%@---->>%@",partChinese,chineseStr);
        chineseStr =[NSString stringWithFormat:@"%@%@",partChinese,chineseStr];
    }
    if([@"" isEqualToString:chineseStr])// 整数部分为 0, 则表达为"零元"
        chineseStr =[NSString stringWithFormat:@"%@",[chineseDigits objectAtIndex:0]];
    else if (negative) // 整数部分不为 0, 并且原金额为负数
        chineseStr =[NSString stringWithFormat:@"负%@",chineseStr];
    
    chineseStr=[NSString stringWithFormat:@"%@元",chineseStr];
    //[chineseStr appendString:@"元"];
    
    if (numFen == 0 && numJiao == 0) {
        chineseStr =[NSString stringWithFormat:@"%@整",chineseStr];
    }
    else if (numFen == 0) { // 0 分，角数不为 0
        
        chineseStr =[NSString stringWithFormat:@"%@%@角",chineseStr,[chineseDigits objectAtIndex:numJiao]];
        
    } else { // “分”数不为 0
        if (numJiao == 0){
            chineseStr =[NSString stringWithFormat:@"%@零%@分",chineseStr,[chineseDigits objectAtIndex:numFen]];
        }
        else{
            chineseStr =[NSString stringWithFormat:@"%@%@角%@分",chineseStr,[chineseDigits objectAtIndex:numJiao],[chineseDigits objectAtIndex:numFen]];
        }
    }
    
    return chineseStr;
    
}

-(CGSize)getSizeoffont:(UIFont *)font withMaxWidth:(double)maxWidth{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil];
    CGSize size= [self  boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size;
}

- (int)getbyte
{
    int strlength = 0;
    char* p = (char*)[self cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[self lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
}

-(NSUInteger)unicodeLength {
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < self.length; i++) {
        unichar uc = [self characterAtIndex: i];
        asciiLength += isascii(uc) ? 1 : 2;
    }
    NSUInteger unicodeLength = asciiLength / 2;
    if(asciiLength % 2) {
        unicodeLength++;
    }
    return unicodeLength;
}

- (NSString*)stringByURLEncoding{
    // NSURL's stringByAddingPercentEscapesUsingEncoding: does not escape
    // some characters that should be escaped in URL parameters, like / and ?;
    // we'll use CFURL to force the encoding of those
    //
    // We'll explicitly leave spaces unescaped now, and replace them with +'s
    //
    // Reference: <a href="%5C%22http://www.ietf.org/rfc/rfc3986.txt%5C%22" target="\"_blank\"" onclick='\"return' checkurl(this)\"="" id="\"url_2\"">http://www.ietf.org/rfc/rfc3986.txt</a>
    
    NSString *resultStr = self;
    
    CFStringRef originalString = (__bridge CFStringRef) self;
    CFStringRef leaveUnescaped = CFSTR(" ");
    CFStringRef forceEscaped = CFSTR("!*'();:@&=+$,/?%#[]");
    
    CFStringRef escapedStr;
    escapedStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                         originalString,
                                                         leaveUnescaped,
                                                         forceEscaped,
                                                         kCFStringEncodingUTF8);
    
    if( escapedStr )
    {
        NSMutableString *mutableStr = [NSMutableString stringWithString:(__bridge NSString *)escapedStr];
        CFRelease(escapedStr);
        
        // replace spaces with plusses
        [mutableStr replaceOccurrencesOfString:@" "
                                    withString:@"%20"
                                       options:0
                                         range:NSMakeRange(0, [mutableStr length])];
        resultStr = mutableStr;
    }
    return resultStr;
}

+ (NSData *)toJSONData:(id)theData{
    if (!theData) {
        return nil;
    }
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}

+ (NSString *)toJSON:(id)obj{
    NSData *data = [NSString toJSONData:obj];
    NSString *jsonString = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    return jsonString;
}




- (id)toArrayOrNSDictionary{
    NSData *jsonData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingAllowFragments
                                                      error:&error];
    
    if (jsonObject != nil && error == nil){
        return jsonObject;
    }else{
        // 解析错误
        return nil;
    }
}


- (NSString *)toUtf8{
    NSString *dataUTF8 = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return dataUTF8;
}

+(NSString *)utf8ToUnicode:(NSString *)string
{
    NSUInteger length = [string length];
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    for (int i = 0;i < length; i++)
    {
        unichar _char = [string characterAtIndex:i];
        //判断是否为英文和数字
        if (_char <= '9' && _char >= '0')
        {
            [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i, 1)]];
        }
        else if(_char >= 'a' && _char <= 'z')
        {
            [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i, 1)]];
            
        }
        else if(_char >= 'A' && _char <= 'Z')
        {
            [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i, 1)]];
            
        }
        else
        {
            [s appendFormat:@"\\u%x",[string characterAtIndex:i]];
        }
    }
    return s;
}

+ (int)convertToInt:(NSString*)strtemp{
    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
}

+ (NSUInteger)uniCodeLengthOfString:(NSString *) text {
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < text.length; i++) {
        unichar uc = [text characterAtIndex: i];
        asciiLength += isascii(uc) ? 1 : 2;
    }
    NSUInteger unicodeLength = asciiLength / 2;
    if(asciiLength % 2) {
        unicodeLength++;
    }
    return unicodeLength;
}

@end
