//
//  CYTSegmentControl.h
//  Cyt-BabyHealth
//
//  Created by hudi on 2017/10/20.
//  Copyright © 2017年 CYT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CYTSegmentType) {
    CYTSegmentType_Constant = 0, // 一屏均分
    CYTSegmentType_Variable, // 可滚动的
    CYTSegmentType_Variable_Center, // 可滚动的 选项在中间的
};

@class CYTSegmentControl;

@protocol CYTSegmentControlDataSource <NSObject>

- (NSArray <NSString *>*)titlesForSegment:(CYTSegmentControl *)segment;

@end

@protocol CYTSegmentControlDelegate <NSObject>

- (void)clickSegment:(CYTSegmentControl *)segment atIndex:(NSInteger)index;

@end

@interface CYTSegmentControl : UIView

@property (nonatomic, assign) CYTSegmentType mSegmentType;
@property (nonatomic, weak) id<CYTSegmentControlDataSource> mDataSource;
@property (nonatomic, weak) id<CYTSegmentControlDelegate> mDelegate;

@property (nonatomic, assign) NSInteger mSelectIndex;           //当前选中的位置

@property (nonatomic, strong) NSArray *mTitleWidths;            //每个item的宽度 CYTSegmentType_Constant时管用
@property (nonatomic, assign) UIEdgeInsets mSegmentInset;       //Segment边距
@property (nonatomic, assign) CGFloat mLineMargin;              //下划线距离两边边距 mLineFixString为NO时管用
@property (nonatomic, assign) BOOL mLineFixString;              //下划线长度跟文字长度一样

@property (nonatomic, strong) DKColorPicker mLineColor;         //下划线的颜色
@property (nonatomic, strong) DKColorPicker mLinebgColor;       //下划线底部颜色，有的需求会要显示浅灰色底儿

@property (nonatomic, strong) DKColorPicker mNormalTextColor;   //文字默认颜色
@property (nonatomic, strong) DKColorPicker mSelectedTextColor; //当前选中item的文字颜色

@property (nonatomic, strong) DKColorPicker mNormalBgColor;     //默认背景颜色
@property (nonatomic, strong) DKColorPicker mSelectedBgColor;   //当前选中的item的背景颜色
@end
