//
//  CYTSegmentCollectionViewCell.m
//  Cyt-BabyHealth
//
//  Created by hudi on 2017/10/20.
//  Copyright © 2017年 CYT. All rights reserved.
//

#import "CYTSegmentCollectionViewCell.h"

@interface CYTSegmentCollectionViewCell()

@end

@implementation CYTSegmentCollectionViewCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.mLabel];
        [self.mLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        self.dk_backgroundColorPicker = DKColorPickerWithKey(C2);
    }
    return self;
    
}

- (UILabel *)mLabel{
    if (_mLabel == nil) {
        _mLabel = [[UILabel alloc]init];
        _mLabel.dk_backgroundColorPicker = DKColorPickerWithKey(C2);
        _mLabel.font = [UIFont systemFontOfSize:16];
        _mLabel.textAlignment = NSTextAlignmentCenter;
        _mLabel.dk_textColorPicker = DKColorPickerWithKey(C8);
    }
    return _mLabel;
}

- (void)reloadCell:(NSString *)text{
    self.mLabel.text = text;
}
@end
