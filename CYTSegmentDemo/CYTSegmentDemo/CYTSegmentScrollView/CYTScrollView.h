//
//  CYTScrollView.h
//  Cyt-BabyHealth
//
//  Created by hudi on 2017/9/27.
//Copyright © 2017年 CYT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CYTScrollView;

@protocol CYTScrollViewDataSource <NSObject>

- (NSInteger)numberOfViewInScrollView:(CYTScrollView *)scrollView;
- (UIView *)scrollView:(CYTScrollView *)scrollView viewAtIndex:(NSInteger)index;

@end

@protocol CYTScrollViewDelegate <NSObject>

@optional
- (void)scrollView:(CYTScrollView *)scrollView didChange:(NSInteger)index;

@end

@interface CYTScrollView : UIView
@property (nonatomic, weak) id <CYTScrollViewDelegate> mDelegate;
@property (nonatomic, weak) id <CYTScrollViewDataSource> mDataSource;
@property (nonatomic, assign) NSInteger mSelectIndex;

- (void)setMSelectIndex:(NSInteger)mSelectIndex animated:(BOOL)animated;

- (void)reloadScrollView;
@end
