//
//  CYTSegmentCollectionViewCell.h
//  Cyt-BabyHealth
//
//  Created by hudi on 2017/10/20.
//  Copyright © 2017年 CYT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CYTSegmentCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *mLabel;

- (void)reloadCell:(NSString *)text;

@end
